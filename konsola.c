#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <limits.h>

//4294967296

unsigned mod10(unsigned x) {
    return x % 10;
}

int parseInt(char *str, unsigned long *long_int_num) {

    unsigned long i = 0, multiply = 1, base = 10;
    unsigned long  length=strlen(str);

    for(i = length; i > 0; i--)    {
        if( ((str[i - 1] - '0' ) >= 0) && ((str[i - 1] - '0' ) <= 9)) {
            *long_int_num = *long_int_num + ((str[i - 1] - '0' ) * multiply);
            multiply *= base;
        }
        else {
        printf("its not a number or it has a negative sign or it's a float \n");
        return 1;
        }
    }
    return 0;
}

int IsItTooLarge(unsigned long long_int_num, unsigned int *unsigned_int_num) {

    if(long_int_num > UINT_MAX ) {
        printf("it's too large\n");
        return 1;
    }
    else {
        *unsigned_int_num = (int) long_int_num;

    }
    return 0;
}

int insertString(char *str) {
    int i =0;

    while((i<98)&&(str[i]=getchar())!='\n'){
        i++;
    }
    str[i]='\0' ;

    if(i>11) {
        printf("it's too large\n");
        return 1;
        }

    return 0;
}

int main() {
    char str[100];

	unsigned long long_int_num=0;
	unsigned int unsigned_int_num=0;
	int mod10Result, parseIntResult, IsItTooLargeResult;
	int insertStringResult;

	insertStringResult = insertString(str);
	if(1 == insertStringResult) {
		printf("ERROR\n");
		return 1;
	}
	else {
 		parseIntResult = parseInt(str,&long_int_num);

		if(1 == parseIntResult) {
			printf("ERROR\n");
		return 1;
		}
		else {
			IsItTooLargeResult = IsItTooLarge(long_int_num, &unsigned_int_num);

			if(1 == IsItTooLargeResult) {
				printf("ERROR\n");
				return 1;
			}
        		else {
            			mod10Result = mod10(unsigned_int_num);
				printf("mod10: %u \n", mod10Result);
        		}
    		}
	}
	return 0;
}

